#!/usr/bin/env ruby
# coding: utf-8

require 'net/http'
require 'uri'
require 'benchmark'

urls = %w( https://repository.duke.edu/dc
           https://repository.duke.edu/dc?f%5Bsubject_facet_sim%5D%5B%5D=Beverages
           https://repository.duke.edu/dc?search_field=all_fields&q=african-americans
           )

n = 5

urls.each do |url|
  u = URI(url)

  Net::HTTP.start(u.host, u.port, use_ssl: true) do |http|
    puts url
    puts '-'*40

    n.times do
      puts Benchmark.realtime { http.request_get(u.request_uri) }
    end

    puts
  end
end
